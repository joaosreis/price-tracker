open Batteries

let get_json soup =
  let open Soup in
  let open Yojson.Basic in
  let open Yojson.Basic.Util in
  let jsons = soup $$ "script[type='application/ld+json']" |>
              filter (fun n -> let j = R.leaf_text n |> from_string in
                       member "@type" j |> to_string = "Product") in
  (match first jsons with
     None -> assert false
   | Some x -> x) |> R.leaf_text |> from_string

let get_price html =
  let open Soup in
  let open Yojson.Basic.Util in
  let soup = parse html in
  let json = get_json soup |> member "offers" in
  let availability = json |> member "availability" |> to_string in
  if String.exists availability "OutOfStock" then
    Price.NoStock
  else
    Price.Stock (member "price" json |> to_string |> float_of_string)

let get_name html =
  let open Soup in
  let open Yojson.Basic.Util in
  let soup = parse html in
  let json = get_json soup in
  member "name" json |> to_string
