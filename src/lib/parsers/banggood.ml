let get_price html =
  let open Soup in
  let soup = parse html in
  match soup $ "meta[itemprop='availability']" |> R.attribute "content" with
    "out of stock" -> Price.NoStock
  | _ -> Price.Stock (soup $ "meta[itemprop='price']" |> R.attribute "content" |>
                      float_of_string)

let get_name html =
  let open Soup in
  let open Yojson.Basic in
  let open Yojson.Basic.Util in
  let soup = parse html in
  let json_string = soup $ "script[type='application/ld+json']" |>
                    R.leaf_text |> String.trim in
  String.sub json_string 0 (String.length json_string - 1) |> from_string |>
  member "name" |> to_string
