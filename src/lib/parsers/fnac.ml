open Batteries

let get_price html =
  let open Soup in
  let open Yojson.Basic in
  let open Yojson.Basic.Util in
  let soup = parse html in
  let json = soup $ "script[type='application/ld+json']" |> R.leaf_text |>
             String.trim |> from_string |> member "offers" in
  let seller = json |> member "seller" |> member "name" |> to_string in
  if not (String.equal seller "Fnac.pt") then (print_endline seller; Price.NoStock)
  else
    let availability = json |> member "availability" |> to_string in
    if String.exists availability "OutOfStock" then
      (print_endline "2"; Price.NoStock)
    else match soup $? "div.f-buyBox-availabilityStatus-available" with
        None -> (print_endline "3"; Price.NoStock)
      | Some _ -> Price.Stock (json |> member "price" |> to_float)

let get_name html =
  let open Soup in
  let open Yojson.Basic in
  let open Yojson.Basic.Util in
  let soup = parse html in
  let json = soup $ "script[type='application/ld+json']" |> R.leaf_text |> String.trim |> from_string in
  member "name" json |> to_string
