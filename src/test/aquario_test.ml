open Utils
open Price

let path_prefix = "../../../../tests/aquario/"

let stock_html = load_file (path_prefix ^ "stock")

let no_stock_html = load_file (path_prefix ^ "no-stock")

let name = "SmartWatch Xiaomi Amazfit Stratos Plus"

let stock () =
  Alcotest.(check price_testable) "same price" (Stock 225.0)
    (Aquario.get_price stock_html)

let no_stock () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Aquario.get_price no_stock_html)

let name () = Alcotest.(check string) "same name"
    name (Aquario.get_name stock_html)

let tests = [
  "stock", stock;
  "no stock", no_stock;
  "name", name
]

let test_set = make_test_set "Aquario" tests
