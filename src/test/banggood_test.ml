open Utils
open Price

let path_prefix = "../../../../tests/banggood/"

let stock_html = load_file (path_prefix ^ "stock")

let no_stock_html = load_file (path_prefix ^ "no-stock")

let name = "Original Xiaomi New 10000mAh Power Bank 2 Dual USB 18W Quick Charge"
           ^ " 3.0 Charger for Mobile Phone"

let stock () =
  Alcotest.(check price_testable) "same price" (Stock 19.99)
    (Banggood.get_price stock_html)

let no_stock () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Banggood.get_price no_stock_html)

let name () = Alcotest.(check string) "same name"
    name (Banggood.get_name stock_html)

let tests = [
  "stock", stock;
  "no stock", no_stock;
  "name", name
]

let test_set = make_test_set "Banggood" tests
