let load_file f =
  let ic = open_in f in
  let n = in_channel_length ic in
  let s = really_input_string ic n in
  let () = close_in ic in
  s
module Price_testable = (struct
  type t = Price.t
  let equal = Price.equal
  let pp fmt x = Format.fprintf fmt "%s" (Price.to_string x)
end)

let price_testable = Alcotest.testable Price_testable.pp Price_testable.equal

let make_test_set name tests =
  List.map (fun (d, t) -> name ^ " " ^ d, `Quick, t) tests
