let () =
  let tests = Worten_test.test_set @ Fnac_test.test_set @
              Globaldata_test.test_set @ Aquario_test.test_set @
              Banggood_test.test_set in
  Alcotest.run "Price Tracker testing" [ "test_set", tests ]
