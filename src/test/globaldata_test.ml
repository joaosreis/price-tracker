open Utils
open Price

let stock_html = load_file "../../../../tests/globaldata/stock"

let no_stock_html = load_file "../../../../tests/globaldata/no-stock"

let name = "Televisor Philips 58\" 58PUS6203 LED Ultra HD 4K SMART TV"

let stock () =
  Alcotest.(check price_testable) "same price" (Stock 499.90)
    (Globaldata.get_price stock_html)

let no_stock () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Globaldata.get_price no_stock_html)

let name () = Alcotest.(check string) "same name"
    name (Globaldata.get_name stock_html)

let tests = [
  "stock", stock;
  "no stock", no_stock;
  "name", name
]

let test_set = make_test_set "Globaldata" tests
