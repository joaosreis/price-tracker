open Utils
open Price

let stock_html = load_file "../../../../tests/worten/stock"

let no_stock_html = load_file "../../../../tests/worten/no-stock"

let no_stock_marketplace_html =
  load_file "../../../../tests/worten/no-stock-marketplace"

let name =
  "Pré-Venda Jogo NINTENDO Switch Fire Emblem: Three Houses (capa provisória)"

let stock () =
  Alcotest.(check price_testable) "same price" (Stock 50.99)
    (Worten.get_price stock_html)

let no_stock () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Worten.get_price no_stock_html)

let no_stock_marketplace () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Worten.get_price no_stock_marketplace_html)

let name () = Alcotest.(check string) "same name"
    name (Worten.get_name stock_html)

let tests = [
  "stock", stock;
  "no stock", no_stock;
  "no stock marketplace", no_stock_marketplace;
  "name", name
]

let test_set = make_test_set "Worten" tests
