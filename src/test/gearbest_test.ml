open Utils
open Price

let path_prefix = "../../../../tests/gearbest/"

let stock_html = load_file (path_prefix ^ "stock")

let no_stock_html = load_file (path_prefix ^ "no-stock")

let name =
  "Xiaomi ROIDMI 3S Bluetooth Carregador de Carro - PRETO"

let stock () =
  Alcotest.(check price_testable) "same price" (Stock 17.32)
    (GearBest.get_price stock_html)

let no_stock () =
  Alcotest.(check price_testable) "same stock" NoStock
    (GearBest.get_price no_stock_html)

let name () = Alcotest.(check string) "same name"
    name (GearBest.get_name stock_html)

let tests = [
  "stock", stock;
  "no stock", no_stock;
  "name", name
]

let test_set = make_test_set "GearBest" tests
