open Utils
open Price

let stock_html = load_file "../../../../tests/fnac/stock"

let no_stock_html = load_file "../../../../tests/fnac/no-stock"

let no_stock_marketplace_html =
  load_file "../../../../tests/fnac/no-stock-marketplace"

let stock_preorder_html = load_file "../../../../tests/fnac/stock-preorder"

let no_stock_preorder_html =
  load_file "../../../../tests/fnac/no-stock-preorder"

let name = "Fire Emblem Echoes: Shadows of Valentia 3DS"

let stock () =
  Alcotest.(check price_testable) "same price" (Stock 44.99)
    (Fnac.get_price stock_html)

let no_stock () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Fnac.get_price no_stock_html)

let no_stock_marketplace () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Fnac.get_price no_stock_marketplace_html)

let stock_preorder () =
  Alcotest.(check price_testable) "same stock" (Stock 59.49)
    (Fnac.get_price stock_preorder_html)

let no_stock_preorder () =
  Alcotest.(check price_testable) "same stock" NoStock
    (Fnac.get_price no_stock_preorder_html)

let name () = Alcotest.(check string) "same name"
    name (Fnac.get_name stock_html)

let tests = [
  "stock", stock;
  "no stock", no_stock;
  "no stock marketplace", no_stock;
  "stock preorder", stock_preorder;
  "no stock preorder", no_stock_preorder;
  "name", name
]

let test_set = make_test_set "Fnac" tests
