FROM ocaml/opam2:alpine-3.9-ocaml-4.07

WORKDIR /home/opam/src

COPY price-tracker.opam .

RUN opam pin add -yn price-tracker . && \
    opam depext price-tracker && \
    opam install --deps-only price-tracker

COPY . .

RUN sudo chown -R opam /home/opam/src && \
    opam config exec make && \
    opam depext -ln price-tracker > depexts

FROM alpine:3.9

WORKDIR /app

COPY --from=0 /home/opam/src/_build/install/default/bin/price-tracker price-tracker.exe

COPY --from=0 /home/opam/src/depexts depexts

RUN cat depexts | xargs apk --update add && rm -rf /var/cache/apk/*

CMD ./price-tracker.exe
