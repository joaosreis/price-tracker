.PHONY: default all utop test test-deps deps clean

default: all

all: deps
	dune build @install

utop: all
	dune utop src/lib

test: test-deps
	dune runtest

deps:
	dune external-lib-deps --missing @install

test-deps:
	dune external-lib-deps --missing @runtest

clean:
	dune clean
